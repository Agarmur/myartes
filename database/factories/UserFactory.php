<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail(),
            'name' => $this->faker->userName,
            'password' => bcrypt('password'), // password
            'nom' => $this->faker->lastName,
            'prenom' => $this->faker->name,
            'section' => $this->faker->company,
            'privecole' => $this->faker->safari,
            'priv' => rand(1,4),
        ];
    }
}
