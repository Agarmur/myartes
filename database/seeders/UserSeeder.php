<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::factory(1)->create();

        User::create([
            'email' => 'batbox.longuepee@gmail.com',
            'name' => 'blonguepee',
            'password' => bcrypt('password'),
            'nom' => 'Longuepee',
            'prenom' => 'Baptiste',
            'section' => 'SN',
            'privecole' => 'Epil',
            'priv' => rand(1,4),
        ]);

    }
}


