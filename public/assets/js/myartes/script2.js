const slidePage = document.querySelector(".slide-page");
const nextBtnFirst = document.querySelector(".firstNext");
const prevBtnSec = document.querySelector(".prev-1");
const nextBtnSec = document.querySelector(".next-1");
const prevBtnThird = document.querySelector(".prev-2");
const nextBtnThird = document.querySelector(".next-2");
const prevBtnFourth = document.querySelector(".prev-3");
const nextBtnFourth = document.querySelector(".next-3");
const prevBtnFive = document.querySelector(".prev-4");
const nextBtnFive = document.querySelector(".next-4");
const prevBtnSix = document.querySelector(".prev-5");
const nextBtnSix = document.querySelector(".next-5");
const prevBtnSeven = document.querySelector(".prev-6");
const nextBtnSeven = document.querySelector(".next-6");
const prevBtnEight = document.querySelector(".prev-7");
const nextBtnEight = document.querySelector(".next-7");
const prevBtnNine = document.querySelector(".prev-8");
const nextBtnNine = document.querySelector(".next-8");
const prevBtnTen = document.querySelector(".prev-9");
const nextBtnTen = document.querySelector(".next-9");
const prevBtnEleven = document.querySelector(".prev-10");
const nextBtnEleven = document.querySelector(".next-10");
const prevBtnTwelve = document.querySelector(".prev-11");
const nextBtnTwelve = document.querySelector(".next-11");
const prevBtnThirteen = document.querySelector(".prev-12");
const nextBtnThirteen = document.querySelector(".next-12");
const prevBtnFourteen = document.querySelector(".prev-13");
const nextBtnFourteen = document.querySelector(".next-13");
const prevBtnFifteen = document.querySelector(".prev-14");
const nextBtnFifteen = document.querySelector(".next-14");
const prevBtnSixteen = document.querySelector(".prev-15");
const submitBtn = document.querySelector(".submit");
const progressText = document.querySelectorAll(".step p");
const progressCheck = document.querySelectorAll(".step .check");
const bullet = document.querySelectorAll(".step .bullet");
let current = 1;

nextBtnFirst.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-25%";
  
});
nextBtnSec.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-50%";
 
});
nextBtnThird.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-75%";
  
});
nextBtnFourth.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-100%";
  bullet[current - 1].classList.add("active");
  progressCheck[current - 1].classList.add("active");
  progressText[current - 1].classList.add("active");
  current += 1;
});
nextBtnFive.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-125%";
  
});
nextBtnSix.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-150%";
  
});
nextBtnSeven.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-175%";
 
});
nextBtnEight.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-200%";
  
});
nextBtnNine.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-225%";
  bullet[current - 1].classList.add("active");
  progressCheck[current - 1].classList.add("active");
  progressText[current - 1].classList.add("active");
  current += 1;
});
nextBtnTen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-250%";
 
});
nextBtnEleven.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-275%";

});
nextBtnTwelve.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-300%";
  
});
nextBtnThirteen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-325%";

});
nextBtnFourteen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-350%";
  bullet[current - 1].classList.add("active");
  progressCheck[current - 1].classList.add("active");
  progressText[current - 1].classList.add("active");
  current += 1;
});

submitBtn.addEventListener("click", function(){
  setTimeout(function(){
    alert("Votre formulaire a bien été enregistré");
    location.replace(accueil.php);
  },800);
});



prevBtnSec.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "0%";

});
prevBtnThird.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-25%";

});
prevBtnFourth.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-50%";

});
prevBtnFive.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-75%";
  bullet[current - 2].classList.remove("active");
  progressCheck[current - 2].classList.remove("active");
  progressText[current - 2].classList.remove("active");
  current -= 1;
});
prevBtnSix.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-100%";

});
prevBtnSeven.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-125%";

});
prevBtnEight.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-150%";

});
prevBtnNine.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-175%";

});
prevBtnTen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-200%";
  bullet[current - 2].classList.remove("active");
  progressCheck[current - 2].classList.remove("active");
  progressText[current - 2].classList.remove("active");
  current -= 1;

});
prevBtnEleven.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-225%";

});
prevBtnTwelve.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-250%";

});
prevBtnThirteen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-275%";

});
prevBtnFourteen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-300%";

});
prevBtnFifteen.addEventListener("click", function(event){
  event.preventDefault();
  slidePage.style.marginLeft = "-325%";
  bullet[current - 2].classList.remove("active");
  progressCheck[current - 2].classList.remove("active");
  progressText[current - 2].classList.remove("active");
  current -= 1;
});






