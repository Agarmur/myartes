<!DOCTYPE html>

  <head>
    <meta charset="utf-8">
    <title>My Artes</title>
    <meta name="viewport" content="width=device-width, initial-scale=0.6">
    <link rel="icon" type="image/png" href="img/title_my_artes.png" />
    <link rel="stylesheet" href="{{ asset('css/myartes/competance.css') }}">
        <link rel="stylesheet" href="{{ asset('css/myartes/numero.css') }}">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </head>
  <body>

     <img class="logo2" src="img/title_caps_go.png">

  <header>

    <a href="accueil.php" class="logo">My Artes</a>

  </header>


  <div class="container">
      <div class="title">Compétences</div>
      <div class="progress-bar">
        <div class="step">
          <p>
</p>
<div class="bullet">
            <span>5</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
<div class="step">
          <p>
</p>
<div class="bullet">
            <span>10</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
<div class="step">
          <p>
</p>
<div class="bullet">
            <span>15</span>
          </div>
<div class="check fas fa-check">
</div>
</div>
</div>
<div class="form-outer">
        <form action="#">
          <div class="page slide-page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/ponctualité.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Ponctualité</div>


</div>

<!-- fin de l'image -->

<!-- slider -->

<div class="wrapper">
      <nav>
        <input type="radio" name="tab1" id="p1" value="-2">
        <input type="radio" name="tab1" id="p2" value="-1">
        <input type="radio" name="tab1" id="p3" value="0" checked>
        <input type="radio" name="tab1" id="p4" value="">
        <input type="radio" name="tab1" id="p5">

        <label for="p1" class="p1"><a href="#">-2</a></label>
        <label for="p2" class="p2"><a href="#">-1</a></label>
        <label for="p3" class="p3"><a href="#">0</a></label>
        <label for="p4" class="p4"><a href="#">1</a></label>
        <label for="p5" class="p5"><a href="#">2</a></label>

        <div class="tab1"></div>
      </nav>
    </div>

<!-- fin slider -->

<!-- bouton next -->

<div class="field">
              <button class="firstNext next">Suivant</button>
        </div>
  </div>

<!-- fin bouton next -->

 <!-- page 2 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/engagement.png" alt="" id="galerie" style="width:65%" />
   <div class="text">Engagement</div>

  </div>

<!-- fin de l'image -->


<!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab2" id="p1_2">
        <input type="radio" name="tab2" id="p2_2">
        <input type="radio" name="tab2" id="p3_2" checked>
        <input type="radio" name="tab2" id="p4_2">
        <input type="radio" name="tab2" id="p5_2">

        <label for="p1_2" class="p1_2"><a href="#">-2</a></label>
        <label for="p2_2" class="p2_2"><a href="#">-1</a></label>
        <label for="p3_2" class="p3_2"><a href="#">0</a></label>
        <label for="p4_2" class="p4_2"><a href="#">1</a></label>
        <label for="p5_2" class="p5_2"><a href="#">2</a></label>

        <div class="tab2"></div>
      </nav>
    </div>


<!-- fin slider -->


<div class="field btns">
              <button class="prev-1 prev">Précédent</button>
              <button class="next-1 next">Suivant</button>
            </div>
</div>

<!-- page 3 -->

<div class="page">
            <div class="title">
</div>

<!-- image -->


<div class="polaroid">

  <img src="img/competance/fiabilité.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Fiabilité</div>

  </div>

<!-- fin de l'image -->

<!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab3" id="p1_3">
        <input type="radio" name="tab3" id="p2_3">
        <input type="radio" name="tab3" id="p3_3" checked>
        <input type="radio" name="tab3" id="p4_3">
        <input type="radio" name="tab3" id="p5_3">

        <label for="p1_3" class="p1_3"><a href="#">-2</a></label>
        <label for="p2_3" class="p2_3"><a href="#">-1</a></label>
        <label for="p3_3" class="p3_3"><a href="#">0</a></label>
        <label for="p4_3" class="p4_3"><a href="#">1</a></label>
        <label for="p5_3" class="p5_3"><a href="#">2</a></label>

        <div class="tab3"></div>
      </nav>
    </div>


<!-- fin slider -->

<div class="field btns">
              <button class="prev-2 prev">Précédent</button>
              <button class="next-2 next">Suivant</button>
            </div>
</div>


<!-- page 4 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/réfléxion.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Réfléxion</div>

  </div>

<!-- fin de l'image -->

<!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab4" id="p1_4">
        <input type="radio" name="tab4" id="p2_4">
        <input type="radio" name="tab4" id="p3_4" checked>
        <input type="radio" name="tab4" id="p4_4">
        <input type="radio" name="tab4" id="p5_4">

        <label for="p1_4" class="p1_4"><a href="#">-2</a></label>
        <label for="p2_4" class="p2_4"><a href="#">-1</a></label>
        <label for="p3_4" class="p3_4"><a href="#">0</a></label>
        <label for="p4_4" class="p4_4"><a href="#">1</a></label>
        <label for="p5_4" class="p5_4"><a href="#">2</a></label>

        <div class="tab4"></div>
      </nav>
    </div>


<!-- fin slider -->


<div class="field btns">
              <button class="prev-3 prev">Précédent</button>
              <button class="next-3 next">Suivant</button>
            </div>
</div>


<!-- page 5 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/propreté.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Propreté</div>
  </div>

<!-- fin de l'image -->


<!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab5" id="p1_5">
        <input type="radio" name="tab5" id="p2_5">
        <input type="radio" name="tab5" id="p3_5" checked>
        <input type="radio" name="tab5" id="p4_5">
        <input type="radio" name="tab5" id="p5_5">

        <label for="p1_5" class="p1_5"><a href="#">-2</a></label>
        <label for="p2_5" class="p2_5"><a href="#">-1</a></label>
        <label for="p3_5" class="p3_5"><a href="#">0</a></label>
        <label for="p4_5" class="p4_5"><a href="#">1</a></label>
        <label for="p5_5" class="p5_5"><a href="#">2</a></label>

        <div class="tab5"></div>
      </nav>
    </div>


<!-- fin slider -->

<div class="field btns">
              <button class="prev-4 prev">Précédent</button>
              <button class="next-4 next">Suivant</button>
            </div>
</div>

<!-- page 6 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->


<div class="polaroid">

  <img src="img/competance/anticipation.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Anticipation</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab6" id="p1_6">
        <input type="radio" name="tab6" id="p2_6">
        <input type="radio" name="tab6" id="p3_6" checked>
        <input type="radio" name="tab6" id="p4_6">
        <input type="radio" name="tab6" id="p5_6">

        <label for="p1_6" class="p1_6"><a href="#">-2</a></label>
        <label for="p2_6" class="p2_6"><a href="#">-1</a></label>
        <label for="p3_6" class="p3_6"><a href="#">0</a></label>
        <label for="p4_6" class="p4_6"><a href="#">1</a></label>
        <label for="p5_6" class="p5_6"><a href="#">2</a></label>

        <div class="tab6"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-5 prev">Précédent</button>
              <button class="next-5 next">Suivant</button>
            </div>
</div>


<!-- page 7 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/autonomie.png" alt="" id="galerie" style="width:65%" />
 <div class="text">Autonomie</div>
  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab7" id="p1_7">
        <input type="radio" name="tab7" id="p2_7">
        <input type="radio" name="tab7" id="p3_7" checked>
        <input type="radio" name="tab7" id="p4_7">
        <input type="radio" name="tab7" id="p5_7">

        <label for="p1_7" class="p1_7"><a href="#">-2</a></label>
        <label for="p2_7" class="p2_7"><a href="#">-1</a></label>
        <label for="p3_7" class="p3_7"><a href="#">0</a></label>
        <label for="p4_7" class="p4_7"><a href="#">1</a></label>
        <label for="p5_7" class="p5_7"><a href="#">2</a></label>

        <div class="tab7"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-6 prev">Précédent</button>
              <button class="next-6 next">Suivant</button>
            </div>
</div>

<!-- page 8 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/communication.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Communication</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab8" id="p1_8">
        <input type="radio" name="tab8" id="p2_8">
        <input type="radio" name="tab8" id="p3_8" checked>
        <input type="radio" name="tab8" id="p4_8">
        <input type="radio" name="tab8" id="p5_8">

        <label for="p1_8" class="p1_8"><a href="#">-2</a></label>
        <label for="p2_8" class="p2_8"><a href="#">-1</a></label>
        <label for="p3_8" class="p3_8"><a href="#">0</a></label>
        <label for="p4_8" class="p4_8"><a href="#">1</a></label>
        <label for="p5_8" class="p5_8"><a href="#">2</a></label>

        <div class="tab8"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-7 prev">Précédent</button>
              <button class="next-7 next">Suivant</button>
            </div>
</div>

<!-- page 9 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/créativité.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Créativité</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab9" id="p1_9">
        <input type="radio" name="tab9" id="p2_9">
        <input type="radio" name="tab9" id="p3_9" checked>
        <input type="radio" name="tab9" id="p4_9">
        <input type="radio" name="tab9" id="p5_9">

        <label for="p1_9" class="p1_9"><a href="#">-2</a></label>
        <label for="p2_9" class="p2_9"><a href="#">-1</a></label>
        <label for="p3_9" class="p3_9"><a href="#">0</a></label>
        <label for="p4_9" class="p4_9"><a href="#">1</a></label>
        <label for="p5_9" class="p5_9"><a href="#">2</a></label>

        <div class="tab9"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-8 prev">Précédent</button>
              <button class="next-8 next">Suivant</button>
            </div>
</div>

<!-- page 10 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/curiosité.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Curiosité</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab10" id="p1_10">
        <input type="radio" name="tab10" id="p2_10">
        <input type="radio" name="tab10" id="p3_10" checked>
        <input type="radio" name="tab10" id="p4_10">
        <input type="radio" name="tab10" id="p5_10">

        <label for="p1_10" class="p1_10"><a href="#">-2</a></label>
        <label for="p2_10" class="p2_10"><a href="#">-1</a></label>
        <label for="p3_10" class="p3_10"><a href="#">0</a></label>
        <label for="p4_10" class="p4_10"><a href="#">1</a></label>
        <label for="p5_10" class="p5_10"><a href="#">2</a></label>

        <div class="tab10"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-9 prev">Précédent</button>
              <button class="next-9 next">Suivant</button>
            </div>
</div>

<!-- page 11 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/initiative.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Participation</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab11" id="p1_11">
        <input type="radio" name="tab11" id="p2_11">
        <input type="radio" name="tab11" id="p3_11" checked>
        <input type="radio" name="tab11" id="p4_11">
        <input type="radio" name="tab11" id="p5_11">

        <label for="p1_11" class="p1_11"><a href="#">-2</a></label>
        <label for="p2_11" class="p2_11"><a href="#">-1</a></label>
        <label for="p3_11" class="p3_11"><a href="#">0</a></label>
        <label for="p4_11" class="p4_11"><a href="#">1</a></label>
        <label for="p5_11" class="p5_11"><a href="#">2</a></label>

        <div class="tab11"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-10 prev">Précédent</button>
              <button class="next-10 next">Suivant</button>
            </div>
</div>

<!-- page 12 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/organisation.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Organisation</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab12" id="p1_12">
        <input type="radio" name="tab12" id="p2_12">
        <input type="radio" name="tab12" id="p3_12" checked>
        <input type="radio" name="tab12" id="p4_12">
        <input type="radio" name="tab12" id="p5_12">

        <label for="p1_12" class="p1_12"><a href="#">-2</a></label>
        <label for="p2_12" class="p2_12"><a href="#">-1</a></label>
        <label for="p3_12" class="p3_12"><a href="#">0</a></label>
        <label for="p4_12" class="p4_12"><a href="#">1</a></label>
        <label for="p5_12" class="p5_12"><a href="#">2</a></label>

        <div class="tab12"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-11 prev">Précédent</button>
              <button class="next-11 next">Suivant</button>
            </div>
</div>

<!-- page 13 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/rapidité.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Rapidité</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab13" id="p1_13">
        <input type="radio" name="tab13" id="p2_13">
        <input type="radio" name="tab13" id="p3_13" checked>
        <input type="radio" name="tab13" id="p4_13">
        <input type="radio" name="tab13" id="p5_13">

        <label for="p1_13" class="p1_13"><a href="#">-2</a></label>
        <label for="p2_13" class="p2_13"><a href="#">-1</a></label>
        <label for="p3_13" class="p3_13"><a href="#">0</a></label>
        <label for="p4_13" class="p4_13"><a href="#">1</a></label>
        <label for="p5_13" class="p5_13"><a href="#">2</a></label>

        <div class="tab13"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-12 prev">Précédent</button>
              <button class="next-12 next">Suivant</button>
            </div>
</div>

<!-- page 14 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/respect.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Respect</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab14" id="p1_14">
        <input type="radio" name="tab14" id="p2_14">
        <input type="radio" name="tab14" id="p3_14" checked>
        <input type="radio" name="tab14" id="p4_14">
        <input type="radio" name="tab14" id="p5_14">

        <label for="p1_14" class="p1_14"><a href="#">-2</a></label>
        <label for="p2_14" class="p2_14"><a href="#">-1</a></label>
        <label for="p3_14" class="p3_14"><a href="#">0</a></label>
        <label for="p4_14" class="p4_14"><a href="#">1</a></label>
        <label for="p5_14" class="p5_14"><a href="#">2</a></label>

        <div class="tab14"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-13 prev">Précédent</button>
              <button class="next-13 next">Suivant</button>
            </div>
</div>


<!-- page 15 -->


<div class="page">
            <div class="title">
</div>

 <!-- image -->

<div class="polaroid">

  <img src="img/competance/Vigilance.png" alt="" id="galerie" style="width:65%" />
  <div class="text">Vigilance</div>

  </div>

<!-- fin de l'image -->


 <!-- slider -->


<div class="wrapper">
      <nav>
        <input type="radio" name="tab15" id="p1_15">
        <input type="radio" name="tab15" id="p2_15">
        <input type="radio" name="tab15" id="p3_15" checked>
        <input type="radio" name="tab15" id="p4_15">
        <input type="radio" name="tab15" id="p5_15">

        <label for="p1_15" class="p1_15"><a href="#">-2</a></label>
        <label for="p2_15" class="p2_15"><a href="#">-1</a></label>
        <label for="p3_15" class="p3_15"><a href="#">0</a></label>
        <label for="p4_15" class="p4_15"><a href="#">1</a></label>
        <label for="p5_15" class="p5_15"><a href="#">2</a></label>

        <div class="tab15"></div>
      </nav>
    </div>


<!-- fin slider -->



<div class="field btns">
              <button class="prev-14 prev">Précédent</button>
              <input class="submit next" type="button" onclick="window.location.href ='accueil.php';" value="Valider"/>
            </div>
</div>





</form>
</div>
</div>

<script src="js/script2.js" type="text/javascript"></script>

  </body>
</html>

