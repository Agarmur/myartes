<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>My Artes</title>
	<link rel="icon" type="image/png" href="{{ asset('media/myartes/title_my_artes.png') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/myartes/index.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/myartes/parametre.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/myartes/demo.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/myartes/menu.css') }}" />

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a076d05399.js"></script>

	<script src="{{ asset('js/myartes/modernizr-2.6.2.min.js') }}"></script>

	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-7243260-2']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>



</head>
<body>




<header>

		<a href="" class="logo">My Artes</a>

		<ul>
			<li><a href="#" class="button" data-modal="modalOne"><i class="fa fa-cog" aria-hidden="true"></i> Gestion</a></li>
			<li><a href="admin.php"><i class="fas fa-user-cog"></i> Admin</a></li>
			<li><a href="#block1"><i class="fa fa-book"></i> Evaluation</a></li>
			<li><a href="#block2"><i class="fa fa-question-circle"></i> A propos</a></li>
            <li><a href="{{ route('web.deconnexion') }}"><i class="fa fa-sign-out"></i>Deconnexion</a></li>
		</ul>

	</header>



	<section class="sectiontiti">

			<img class="img" src="{{ asset('media/myartes/title_caps_go.png') }}">

	</section>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</head>

	<div id="modalOne" class="modal">
      <div class="modal-content">
        <div class="pop-form">
          <a class="close">&times;</a>
          <form action="/">
            <div>
				<input type="button" onclick="window.location.href ='inscription_eleve.php';" value="Ajouter un élève"/>
				<input type="button" onclick="window.location.href ='modifier_eleve.php';" value="Modifier un élève"/>
				<input type="button" onclick="window.location.href ='inscription.php';" value="Supprimer un élève"/>
			    <input type="button" onclick="window.location.href ='inscription_prof.php';" value="Ajouter un professeur"/>
				<input type="button" onclick="window.location.href ='inscription.php';" value="Modifier un professeur"/>
				<input type="button" onclick="window.location.href ='inscription.php';" value="Supprimer un professeur"/>


            </div>
            <div>
            </div>
          </form>
        </div>
      </div>
    </div>
    </div>

    <script>
      var modalBtns = [...document.querySelectorAll(".button")];
      modalBtns.forEach(function(btn){
        btn.onclick = function() {
          var modal = btn.getAttribute('data-modal');
          document.getElementById(modal).style.display = "block";
        }
      });
      var closeBtns = [...document.querySelectorAll(".close")];
      closeBtns.forEach(function(btn){
        btn.onclick = function() {
          var modal = btn.closest('.modal');
          modal.style.display = "none";
        }
      });
      window.onclick = function(event) {
        if (event.target.className === "modal") {
          event.target.style.display = "none";
        }
      }
    </script>

<body>

	<script type="text/javascript">
		var scroll = document.querySelector('.curve');
		window.addEventListener('scroll', function(){
			var value = 1 + window.scrollY/-500;
			scroll.style.transform = `scaleY(${value})`;
			window.onscroll = function() {myFunction()};
			})

	</script>


	<script type="text/javascript">
		window.addEventListener("scroll", function(){
			var header = document.querySelector("header");
			header.classList.toggle("sticky", window.scrollY > 0);

		})
	</script>





<div class="user-section">
	<h1 class="Profil">Profil</h1>
	 <div class="border_user"></div>

		<div class="container_user">
			<div class="card_user">
				<div class="content_user">
					<div class="imgBx_user"><img src="{{ asset('media/myartes/pp/pp.png') }}"></div>
					<div class="contentBx_user">
						<h3>{{ auth()->user()->prenom }} {{ auth()->user()->nom }}<br><span>{{ auth()->user()->section }}</span></h3>
					</div>
				</div>

				<ul class="sci">
				<li style="--i:1">
					<a href="#" class="bouton">Messagerie</a>
				</li>

			</ul>
			</div>
		</div>
	</div>

</div>


<div class="block1" id="block1">

	 <div class="container">
  	<div class="card">
  		<div class="box">
  			<div class="content">
  				<h3>Evaluation</h3>
  				<p>Cliquez sur le bouton ci-dessous pour lancer une auto-évaluation</p>
  				<a href="{{ route('web.competance') }}">Evaluation</a>
  			</div>
  		</div>
  	</div>

  	  <div class="card">
  		<div class="box">
  			<div class="content">
  				<h3>Résultats</h3>
  				<p>Cliquez sur le bouton ci-dessous pour voir vos résultats</p>
  				<a href="{{ route('web.resultat') }}">Résultat</a>
  			</div>
  		</div>
  	</div>

  	  <div class="card">
  		<div class="box">
  			<div class="content">
  				<h3>Classe</h3>
  				<p>Cliquez sur le bouton ci-dessous pour voir les résultats des élèves de votre classe</p>
  				<a href="">Classe</a>
  			</div>
  		</div>
  	</div>

  	</div>
  </div>

  <div class="block2" id="block2">

<div class="A_propos-section">
	<h1>A propos de nous</h1>
	 <div class="border"></div>

		<div class="container2">
			<div class="card">
				<div class="content2">
					<div class="imgBx"><img src="{{ asset('media/myartes/pp/thibaut.png') }}"></div>
					<div class="contentBx">
						<h3>Gabet Thibaut<br><span>Développeur Front</span></h3>
					</div>
				</div>

				<ul class="sci">
				<li style="--i:1">
					<a href="https://www.linkedin.com/in/gabet-thibaut-8393a4207/" target="_blank"><img src="{{ asset('media/myartes/font/linkedin-brands.png') }}" height="25px" width="24px"></a>
				</li>
				<li style="--i:2">
					<a href="mailto:info@myartes.fr"><img src="{{ asset('media/myartes/font/envelope-solid.png') }}" height="24px" width="24px"></a>
				</li>
			</ul>
			</div>



			<div class="card">
				<div class="content2">
					<div class="imgBx"><img src="{{ asset('media/myartes/pp/clovis.png') }}"></div>
					<div class="contentBx">
						<h3>Debrandt Clovis<br><span>Développeur Back</span></h3>
					</div>
				</div>

				<ul class="sci">
				<li style="--i:1">
					<a href="https://www.linkedin.com/in/clovis-debrandt/" target="_blank"><img src="{{ asset('media/myartes/font/linkedin-brands.png') }}" height="25px" width="24px"></a>
				</li>
				<li style="--i:2">
					<a href="mailto:info@myartes.fr"><img src="{{ asset('media/myartes/font/envelope-solid.png') }}" height="24px" width="24px"></a>
				</li>
			</ul>
			</div>




			<div class="card">
				<div class="content2">
					<div class="imgBx"><img src="{{ asset('media/myartes/pp/baptiste.png') }}"></div>
					<div class="contentBx">
						<h3>Longuepee Baptiste<br><span>Développeur Front et Back</span></h3>
					</div>
				</div>

				<ul class="sci">
				<li style="--i:1">
					<a href="https://www.linkedin.com/in/baptiste-longuepee-6953a4207/" target="_blank"><img src="{{ asset('media/myartes/font/linkedin-brands.png') }}" height="25px" width="24px"></a>
				</li>
				<li style="--i:2">
					<a href="mailto:info@myartes.fr"><img src="{{ asset('media/myartes/font/envelope-solid.png') }}" height="24px" width="24px"></a>
				</li>
			</ul>
			</div>



			<div class="card">
				<div class="content2">
					<div class="imgBx"><img src="{{ asset('media/myartes/pp/cycy.png') }}"></div>
					<div class="contentBx">
						<h3>Douillet Cyriaque<br><span>Designer Web</span></h3>
					</div>
				</div>

				<ul class="sci">
				<li style="--i:1">
					<a href="https://www.linkedin.com/in/cyriaque-douillet-95a771207/" target="_blank"><img src="{{ asset('media/myartes/font/linkedin-brands.png') }}" height="25px" width="24px"></a>
				</li>
				<li style="--i:2">
					<a href="mailto:info@myartes.fr"><img src="{{ asset('media/myartes/font/envelope-solid.png') }}" height="24px" width="24px"></a>
				</li>
			</ul>
			</div>

</did>




<div class="menu">


				<button class="cn-button" id="cn-button">+</button>
				<div class="cn-wrapper" id="cn-wrapper">
				    <ul>
				      <li></li>
				      <li><a href="#" class="button" data-modal="modalOne"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
				      <li><a href="#block1"><i class="fa fa-book"></i></a></li>
				      <li><a href="#block2"><i class="fa fa-question-circle"></i></a></li>

				     </ul>
				</div>
				<div id="cn-overlay" class="cn-overlay"></div>

			</div>
		</div>
		<script src="{{ asset('js/myartes/polyfills.js') }}"></script>
		<script src="{{ asset('js/myartes/demo1.js') }}"></script>

		<script>
			var modalBtns = [...document.querySelectorAll(".button")];
			modalBtns.forEach(function(btn){
			  btn.onclick = function() {
				var modal = btn.getAttribute('data-modal');
				document.getElementById(modal).style.display = "block";
			  }
			});
			var closeBtns = [...document.querySelectorAll(".close")];
			closeBtns.forEach(function(btn){
			  btn.onclick = function() {
				var modal = btn.closest('.modal');
				modal.style.display = "none";
			  }
			});
			window.onclick = function(event) {
			  if (event.target.className === "modal") {
				event.target.style.display = "none";
			  }
			}
		  </script>


</body>
</html>
