
<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Résultat</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.4">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/resultat_bak.php"/>
    <script src="https://code.jquery.com/jquery-3.1.0.slim.min.js" type="text/javascript"></script>
    <style>
    body { background-color:#fafafa; font-family:'Roboto';}
    h1 { margin:150px auto 30px auto;}
    </style>
</head>

<body>
<img class="logo2" src="img/title_caps_go.png">
<header>
    <a href="accueil.php" class="logo">My Artes</a>
</header>

<div class="jquery-script-center">
	<div class="jquery-script-ads">
		<script type="text/javascript"
			src="https://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
	<div class="jquery-script-clear"></div>
</div>



<div class="skill-bars">

	<div id="bar1" class="barfiller">
		  <span>Ponctualité</span>
			<div class="progress-line p1 fill" data-percentage="">
		  		<span></span>
			</div>
	</div>

	<div id="bar2" class="barfiller">
		<span>Engagement</span>
		  	<div class="progress-line p2 fill" data-percentage="" >
				<span></span>
		  	</div>
	</div>

	<div id="bar3" class="barfiller">
		<span>Fiabilité</span>
		  	<div class="progress-line p3 fill" data-percentage="" >
				<span></span>
		  	</div>
  	</div>

  	<div id="bar4" class="barfiller">
		<span>Réfléxion</span>
	  		<div class="progress-line p4 fill" data-percentage="" >
				<span></span>
	  		</div>
	</div>

	<div id="bar5" class="barfiller">
		<span>Propreté</span>
		  	<div class="progress-line p5 fill" data-percentage="" >
				<span></span>
		  	</div>
	</div>

	<div id="bar6" class="barfiller">
		<span>Anticipation</span>
		  <div class="progress-line p6 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>

	<div id="bar7" class="barfiller">
		<span>Autonomie</span>
		  <div class="progress-line p7 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>

	<div id="bar8" class="barfiller">
		<span>Communication</span>
		  <div class="progress-line p8 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>

	<div id="bar9" class="barfiller">
		<span>Créativité</span>
		  <div class="progress-line p9 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>

	<div id="bar10" class="barfiller">
		<span>Curiosité</span>
		  <div class="progress-line p10 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>


	<div id="bar11" class="barfiller">
		<span>Participation</span>
		  <div class="progress-line p11 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>

	<div id="bar12" class="barfiller">
		<span>Organisation</span>
		  <div class="progress-line p12 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>


	<div id="bar13" class="barfiller">
		<span>Rapidité</span>
		  <div class="progress-line p13 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>

	<div id="bar14" class="barfiller">
		<span>Respect</span>
		  <div class="progress-line p14 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>


	<div id="bar15" class="barfiller">
		<span>Vigilance</span>
		  <div class="progress-line p15 fill" data-percentage="" >
				<span></span>
		  </div>
	</div>


</div>




<script src="js/jquery.barfiller.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){

	$('#bar1').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar2').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar3').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar4').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar5').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar6').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar7').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar8').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar9').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar10').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar11').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar12').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar13').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar14').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
	$('#bar15').barfiller({ barColor: 'linear-gradient(-90deg, #f5ce62, #e85a19)', duration: 3000 });
});

</script>

<script type="text/javascript">
    window.addEventListener("scroll", function(){
      var header = document.querySelector("header");
      header.classList.toggle("sticky", window.scrollY > 0);

    })
  </script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
