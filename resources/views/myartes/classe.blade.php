<!DOCTYPE  html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Artes</title>
    <link rel="icon" type="image/png" href="img/title_my_artes.png" />
    <link rel="stylesheet" href="css/classe.css">
</head>
<body>


   <img class="logo2" src="img/title_caps_go.png">

  <header>
    <a href="accueil.php" class="logo">My Artes</a>
  </header>


  <div class="container">

<div class="main-card">

  <div class="cards">
    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/baptistev.png" alt="">
       </div>
       <div class="details">
         <div class="name">Vaesken Baptiste</div>
         <div class="job">Terminal SN risc</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/augustin.png" alt="">
       </div>
       <div class="details">
         <div class="name">Carrette Augustin</div>
         <div class="job">Terminal SN risc</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/antoine.png" alt="">
       </div>
       <div class="details">
         <div class="name">Klein Antoine</div>
         <div class="job">Terminal SN risc</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/arthur.png" alt="">
       </div>
       <div class="details">
         <div class="name">Gantois Arthur</div>
         <div class="job">Terminal SN risc</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/djibril.png" alt="">
       </div>
       <div class="details">
         <div class="name">Massrour Djibril</div>
         <div class="job">Classe</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/sylvain.png" alt="">
       </div>
       <div class="details">
         <div class="name">Vansteenkiste Sylvain</div>
         <div class="job">Classe</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/julian.png" alt="">
       </div>
       <div class="details">
         <div class="name">Brasseur Julian</div>
         <div class="job">Classe</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/galdric.png" alt="">
       </div>
       <div class="details">
         <div class="name">Tingaud Galdric</div>
         <div class="job">Classe</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>

    <div class="card">
     <div class="content">
       <div class="img">
         <img src="img/pp/raphael.png" alt="">
       </div>
       <div class="details">
         <div class="name">Torris Raphaël</div>
         <div class="job">Classe</div>
       </div>
         <a class="boutton" href="#">Résultat</a>
     </div>
    </div>        


  </div>
</div>
</div>

<script type="text/javascript">
    window.addEventListener("scroll", function(){
      var header = document.querySelector("header");
      header.classList.toggle("sticky", window.scrollY > 0);

    })
  </script>


</body>
</html>
