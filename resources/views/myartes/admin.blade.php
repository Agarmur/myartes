<?php
$bdd = new PDO('mysql:host=myartenbdd.mysql.db;dbname=myartenbdd;', 'myartenbdd', 'Myartes403');
$allnom = $bdd->query('SELECT * FROM admin ORDER BY id ASC');
if(isset($_GET['s']) AND !empty($_GET['s'])){
	$myartes = htmlspecialchars($_GET['s']);
	$allnom = $bdd->query('SELECT id, nom, site FROM admin WHERE nom LIKE "%'.$myartes.'%" ORDER BY id ASC');
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>My Artes	</title>
	<link rel="icon" type="image/png" href="img/title_my_artes.png" />
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel='stylesheet' type="text/css" href="css/style.php">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="js/modernizr-2.6.2.min.js"></script>

</head>
<body>
<header>
		<a href="accueil.php" class="logo">My Artes</a>

<style>
.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}
.topnav a:hover {
  background-color: #ddd;
  color: black;
}
.topnav a.active {
  background-color: #2196F3;
  color: white;
}
.topnav .search-container {
  float: right;
}
.topnav input[type=text] {
  padding: 6px;
  font-size: 17px;
  border: 1px solid #ccc; 
  border-radius: 10px;
}
@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}
</style>

<div class="topnav">
  
  <div class="search-container">
    <form method="GET">
      <input type="text" placeholder="Recherche" name="s">

    </form>
  </div>
</div>

</header>

<section class="admin_important">
</section>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

</head>

<body>
 
 <section class="resultat_recherche">
 <?php
	if($allnom->rowCount() > 0){
		while($admin = $allnom->fetch()){
			?>
				<div class="card">
				<div class="box">
				<div class="content">
					<h2><?= $admin['id']; ?></h2>
					<h3><?= $admin['nom']; ?></h3>
				 	<a href="<?= $admin['site']; ?>">Parametre</a>
				</div>
				</div>
				</div>
			<?php
	}
}else{
	?>
	<p>Ce site n'existe pas</p>
	<?php
}

?>
 </section>

	<script type="text/javascript">
		var scroll = document.querySelector('.curve');
		window.addEventListener('scroll', function(){
			var value = 1 + window.scrollY/-500;
			scroll.style.transform = `scaleY(${value})`;
			window.onscroll = function() {myFunction()};
			})
	</script>

	<script type="text/javascript">
		window.addEventListener("scroll", function(){
			var header = document.querySelector("header");
			header.classList.toggle("sticky", window.scrollY > 0);
		})
	</script>

</body>
</html>