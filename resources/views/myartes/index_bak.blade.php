<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script
        src="https://kit.fontawesome.com/64d58efce2.js"
        crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="{{ asset('css/myartes/login.css') }}" />
    <title>My Artes</title>
    <link rel="icon" type="image/png" href="{{ asset('media/myartes/title_caps_go.png') }}" />
</head>
<body id="kt_body">
<div class="container">
    <div class="forms-container" id="kt_login">
        <div class="signin-signup">

            <form method="POST" class="sign-in-form" id="kt_login_signin_form" action="{{ route('web.authentification') }}" data-urlredirect="{{ route('web.accueil') }}">
                @csrf
                <p class="error"></p>
                <h2 class="title">Connexion</h2>
                <div class="input-field">
                    <i class="fas fa-user"></i>
                    <input type="text" name="username" placeholder="Email" />
                </div>
                <div class="input-field">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="password" placeholder="Mot de passe" />
                </div>
                <button id="kt_login_signin_submit" class="btn solid">Connexion</button>
            </form>
        </div>
    </div>

    <div class="panels-container">
        <div class="panel left-panel">

            <img src="{{ asset('media/myartes/title_caps_go.png') }}" class="image" alt="" />
        </div>

    </div>
</div>

<script>
    var HOST_URL = "{{ env('APP_URL') }}";
</script>

<script src="{{ asset('js/extranet.login.js') }}"></script>

</body>
</html>
