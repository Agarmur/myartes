<?php
require('connexion.php');

if(isset($_POST['adduser']))
{
	if($_POST['password1'] == $_POST['password2'])
	{
		$reqadduser = $bdd->stmt_init();
		$reqadduser = $bdd->prepare(" 	INSERT INTO membres (email, login, password, nom, prenom, section, privecole, priv)
										VALUES 				                      (    ?,     ?,        ?,   ?,      ?,       ?,         ?,    ?);
									");
		$reqadduser->bind_param('ssssssss' , $_POST['email'],$_POST['login'],$_POST['password1'],$_POST['nom'],$_POST['prenom'],$_POST['section'],$_POST['privecole'],$_POST['priv']);
		$reqadduser->execute();
	}
	header("Location: accueil.php");
}
?>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  <title>My Artes</title>
    <link rel="stylesheet" href="css/inscription.css">
  </head>
  <body>

<header>
    
    <a href="accueil.php" class="logo">My Artes</a>

</header>


<div class="wrapper">
    <div class="title">
		Ajoutez un élève
	</div>
	<form class="adduser" method="post" action="inscription.php">
		<div class="field">
          <input type="text" name="nom" required>
          <label>Nom</label>
        </div>
        <div class="field">
          <input type="text" name="prenom"  required>
          <label>Prénom</label>
        </div>
        <div class="field">
          <input type="email" name="email" required>
          <label>Email</label>
        </div>
        <div class="field">
          <input type="text" name="login"  required>
          <label>Utilisateur</label>
        </div>
        <div class="field">
          <input type="text" name="section" required>
          <label>Section</label>
        </div>
		<div class="field">
          <input type="password" name="password1" required>
          <label>Mot de passe</label>
        </div>
        <div class="field">
          <input type="password" name="password2" required>
          <label>Confirmez le mot de passe</label>
        </div>
        <div class="field">
          <input type="text" name="privecole" required>
          <label>Nom école</label>
        </div>
        <div class="priv">Privilèges : </div>
			<div class="admtext">
				<label><input type="radio" name="priv" value="eleve" checked> Élève</label>
				<br>
				<label><input type="radio" name="priv" value="prof"> Professeur</label>
				<br>
				<label><input type="radio" name="priv" value="directeur"> Directeur</label>
				<br>
				<label><input type="radio" name="priv" value="fonda"> Fondateur</label>
			</div>
		<div class="field">
        	<input type="submit" name="adduser" value="Ajouter"/>
        </div>
	</form>
	<?
     	if(isset($erreur))
     	{
     		echo $erreur;
     	}   	
    ?>
</div>

</body>
</html>