<?php

namespace App\Http\Controllers\Myartes\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class DeconnexionController extends Controller
{
    use AuthenticatesUsers;

    public function __invoke(Request $request)
    {
        return $this->logout($request);
    }

    public function loggedOut(Request $request): \Illuminate\Http\RedirectResponse
    {
        return redirect()->route('web.deconnexion');
    }
}
