<?php

namespace App\Http\Controllers\MyArtes;

use App\Http\Controllers\Controller;

class HomePageController extends Controller
{
    public function __invoke()
    {
        return view('myartes.accueil');
    }
}
