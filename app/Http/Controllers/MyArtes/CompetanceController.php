<?php

namespace App\Http\Controllers\Myartes;

use App\Http\Controllers\Controller;

class CompetanceController extends Controller
{
    public function __invoke()
    {
        return view('myartes.competance');
    }
}
