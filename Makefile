projet=myartes

#KNOWN_TARGETS = artisan composer npm
#ARGS := $(filter-out $(KNOWN_TARGETS),$(MAKECMDGOALS))

SUPPORTED_COMMANDS := artisan composer
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  COMMAND_ARGS := $(subst :,\:,$(COMMAND_ARGS))
  $(eval $(COMMAND_ARGS):;@:)
endif

help:                 ##Afficher l'aide sur le fichier
	@cat Makefile | grep "##." | sed '2d;s/##//;s/://'
dev-start:            ##Démarrer l'instance Developpement Docker
	@sudo docker-compose -f docker/developpement/docker-compose.yml up -d --force-recreate
dev-build-docker:     ##Build l'instance Developpement Docker
	@docker-compose -f docker/developpement/docker-compose.yml up --build
dev-stop:             ##Arreter l'instance Developpement Docker
	@sudo docker-compose -f docker/developpement/docker-compose.yml down
prod-start:           ##Démarrer l'instance Production Docker
	@sudo docker-compose -f docker/developpement/docker-compose.yml up -d --force-recreate
prod-build-docker:    ##Build l'instance Production Docker
	@docker-compose -f docker/developpement/docker-compose.yml up --build
prod-stop:            ##Arreter l'instance Production Docker
	@sudo docker-compose -f docker/developpement/docker-compose.yml down
ssl-start:
	docker-compose -f docker/docker-compose-ssl.yml up -d
ssl-stop:
	docker-compose -f docker/docker-compose-ssl.yml down
ssl-exec:
	docker exec -it --user www-data "le14-ssl" bash
ssl-create:
	docker run -it --rm \
	      -v ~/le14-web/docker/letsencrypt:/etc/letsencrypt \
	      -v ~/le14-web/docker/letsencrypt-data:/data/letsencrypt \
	      -v /var/log/letsencrypt:/var/log/letsencrypt \
	      deliverous/certbot \
	      certonly \
	      --webroot --webroot-path=/data/letsencrypt \
	      -d www.le14-orchies.com
ssl-renew:
	docker run -t --rm \
	      -v ~/le14-web/docker/letsencrypt:/etc/letsencrypt \
	      -v ~/le14-web/docker/letsencrypt-data:/data/letsencrypt \
	      -v /var/log/letsencrypt:/var/log/letsencrypt \
	      deliverous/certbot \
	      renew \
	      --webroot --webroot-path=/data/letsencrypt
certificate-local-create:
	rm -Rf docker/local-ssl
	mkdir -p docker/local-ssl docker/local-ssl/certs docker/local-ssl/private
	sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./docker/local-ssl/private/nginx-selfsigned.key -out ./docker/local-ssl/certs/nginx-selfsigned.crt
	sudo openssl dhparam -out docker/local-ssl/certs/dhparam.pem 2048
exec:                 ##Bash sur l'instance docker php
	sudo docker exec -it --user www-data "$(projet)-php" bash
mariadb:              ##Bash sur l'instance docker mariadb
	@sudo docker exec -it "$(projet)-mariadb" bash
npmdev:	             ##Compile les assets en mode dev
	docker exec -it --user www-data "$(projet)-php" bash -c "npm run dev"
npminstall:           ##Installation des packages pour les assets
	docker exec -it --user www-data "$(projet)-php" bash -c "npm install --frozen-lockfile"

.PHONY: artisan
artisan:              ##Console Laravel
	docker exec -it --user www-data "$(projet)-php" bash -c "php artisan $(COMMAND_ARGS)"

clear:                ##Nettoyage des caches Laravel
	docker exec -it --user www-data "$(projet)-php" bash -c "php artisan route:clear"
	docker exec -it --user www-data "$(projet)-php" bash -c "php artisan config:clear"
	docker exec -it --user www-data "$(projet)-php" bash -c "php artisan cache:clear"
	rm -Rf storage/framework/cache/data/*
composer:             ##Installation des composants et librairies php du projet : install, update [package_name], clearcache, dumpautoload
	docker exec -it --user www-data "$(projet)-php" bash -c "sh ./docker/composer.sh $(COMMAND_ARGS)"
ssh:                  ##Connexion à l'instance de production
	ssh -t projects@178.32.126.43 "cd /home/projects/le14-web; zsh"



