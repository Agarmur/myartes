<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Myartes\Auth\ConnexionController;
use App\Http\Controllers\Myartes\Auth\DeconnexionController;
use App\Http\Controllers\Myartes\CompetanceController;
use App\Http\Controllers\MyArtes\HomePageController;
use App\Http\Controllers\Myartes\ResultatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/deconnexion', DeconnexionController::class)->name('web.deconnexion');
Route::get('/connexion', ConnexionController::class)->name('web.connexion');

Route::post('/authentification', LoginController::class)->name('web.authentification');

Route::middleware(['auth'])->group(function () {
    Route::get('/', HomePageController::class)->name('web.accueil');
});

Route::get('/competance', CompetanceController::class)->name('web.competance');
Route::get('/resultat', ResultatController::class)->name('web.resultat');



